const os = require('os')
const fs = require('fs')
const { sep } = require('path')
const https = require('https')
const { Canvas, createCanvas, createImageData, ImageData, Image, loadImage } = require('canvas')

const tf = require('@tensorflow/tfjs-node')
const faceapi = require('face-api.js')

const ffmpegStatic = require('ffmpeg-static')
const ffmpeg = require('ffmpeg')

class FaceAnalysis {
  constructor(videoUrl) {
    this.videoUrl = videoUrl //any upstream url
    
    this.faceDetectionOptions = new faceapi.SsdMobilenetv1Options({minConfidence: .35})
    
    this.modelsInited = false
    this.modelUrl = `${process.env.APP_HOST}/static/models/`

    this.frameDetections = []
  }

  async _initModels() {
    await faceapi.loadSsdMobilenetv1Model(this.modelUrl)
    await faceapi.loadFaceRecognitionModel(this.modelUrl)
    await faceapi.loadFaceExpressionModel(this.modelUrl)

    faceapi.env.monkeyPatch({ fetch: fetch, Canvas, ImageData, Image }) // Polyfills for Canvas/Images in node server
  }  

  _tempVideo() {
    return new Promise((resolve, reject) => {
      const tempDir = fs.mkdtempSync(`${os.tmpdir()}${sep}`)
      const tempFilePath = `${tempDir}/${this.videoUrl.split('/').slice(-1)}`

      try {
        const file = fs.createWriteStream(tempFilePath)
        const request = https.get(this.videoUrl, (response) => {
          response.pipe(file)
          file.on('finish', () => {
            file.close(() => {
              return resolve(tempFilePath)
            })
          })
        }).on('error', (err) => {
          fs.unlink(tempFilePath)
          return recet(err)
        })
      } catch (err) {
        console.error('Face Analysis ._tempVideo() write to fs exception: \n', err)
        return reject(err)
      }
    })
  }

  _tempFrames(videoPath) {
    return new Promise((resolve, reject) => {
      const tempDir = fs.mkdtempSync(`${os.tmpdir()}${sep}`)

      try {
        new ffmpeg(videoPath).then((video) => {
          const ffmpegOptions = {
            frame_rate: 3,
            file_name: ''
          }

          video.fnExtractFrameToJPG(tempDir, ffmpegOptions, (error, files) => {
            if (!error) {
              return resolve({files: files, videoDimensions: video.metadata.video.resolutionSquare})
            } else {
              console.log('Face Analysis FFMpeg Failed to extract frames...', error)
              return reject()
            }
          })
        }, (err) => {
          console.log('Error: ' + err);
        })
      } catch(err) {
        console.error('Face Analysis ._tempFrames() ffmpeg frame rate error: \n', err)
      }
    })    
  }

  async analyzeVideo() {
    const start = new Date().getTime()

    if (!this.modelsInited) {
      await this._initModels()
      this.modelsInited = true
    }

    return new Promise((resolve, reject) => {
      this._tempVideo()
        .then(async (videoPath) => {
          const framesRes = await this._tempFrames(videoPath)

          framesRes.files = framesRes.files.sort((a, b) => {
            let _a = a.split('/').slice(-1)
            let _b = b.split('/').slice(-1)        

            _a = _a[0].replace(/_/g , "")
            _b = _b[0].replace(/_/g , "")         

            _a = parseInt(_a)
            _b = parseInt(_b) 

            return _a - _b
          })
          
          //chunk data
          const chunkedFrames = []
          const batchSize = 20

          for (let i = 0; i < framesRes.files.length; i += batchSize) {
            chunkedFrames.push(framesRes.files.slice(i, i + batchSize))
          }
          
          //reduce promises in chunks
          const promiseChain = await chunkedFrames.reduce((chain, batch, batchIndex) => {
            return chain.then(async () => await Promise.all(
              batch.map(async (file, index) => {
                console.log("Processing batch #", index, file)

                const canvas = createCanvas(framesRes.videoDimensions.w, framesRes.videoDimensions.h)
                const ctx = canvas.getContext('2d')
                const img = await loadImage(file)

                ctx.drawImage(img, 0, 0)

                const detectionsWithExpressions = await faceapi.detectAllFaces(canvas, this.faceDetectionOptions).withFaceExpressions()
                this.frameDetections.push({
                  frame: (batchIndex * batchSize) + index,
                  frameDimensions: framesRes.videoDimensions,
                  detections: detectionsWithExpressions
                })
              })
            ))
          }, Promise.resolve())

          this.frameDetections.sort((a,b) => {
            return a.frame - b.frame
          })

          return resolve({"frameDetections": this.frameDetections})
        }).catch((err) => {
          console.log("analyze video failed: \n", err)
          return reject()
        })
    })
  }
}

module.exports = FaceAnalysis