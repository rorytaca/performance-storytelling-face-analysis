const path = require('path')
const express = require('express');
const bodyParser = require('body-parser')
global.fetch = require('node-fetch');

const app = express();
// security
app.disable('x-powered-by');
// configs
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
require('dotenv').config({
  path: path.join(__dirname, '.env'),
  systemvars: true
})
app.use('/static', express.static('static'))

app.get('/', (req, res) => {
  res.send('PS FACE ANALYSIS MICROSERVICE');
});

// Face analysis endpoint
const FaceAnalysis = require('./lib/face-analysis');
app.post('/process', (req, res) => {
  if (!req.body.video_url) {
    res.statusMessage = 'Missing parameter: videoUrl'
    console.error('[API] POST /process error: \n', res.statusMessage)
    return res.status(400).end()
  }

  const fa = new FaceAnalysis(req.body.video_url)

  console.log("initing video analysis");
  fa.analyzeVideo()
    .then(faceResults => {
      console.log("video analysis complete");
      console.log(faceResults);

      res.status(200).send({
        "status": "success",
        "facial_analysis_payload": faceResults.frameDetections
      })
    }).catch(err => {
      console.error('[API] POST /process error: \n', err)
      res.statusMessage = 'Something went wrong while attempting to process video'
      return res.status(500).end()
    });
});

// Listen to the App Engine-specified port, or 8080 otherwise
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}...`);
});

